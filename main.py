import numpy as np
import pandas as pd
import plotly.graph_objs as go
import plotly.offline as py

#Load data
# data_csv = pd.read_csv('BasicCompanyDataAsOneFile-2018-12-05.csv', low_memory=False, nrows=200000)
data_csv = pd.read_csv('BasicCompanyDataAsOneFile-2018-12-05.csv', low_memory=False)
#Remove some columns i will nor use
data_csv.drop(columns=['CompanyName', 'RegAddress.CareOf', 'RegAddress.POBox', 'RegAddress.AddressLine1',
                       ' RegAddress.AddressLine2', 'RegAddress.PostTown', 'RegAddress.PostCode', 'URI'], inplace=True)

def process_country_features():
    data_csv['RegAddress.Country'] = data_csv['RegAddress.Country'].replace('VIRGIN ISLANDS, BRITISH','VIRGIN ISLANDS')
    data_csv['RegAddress.Country'] = data_csv['RegAddress.Country'].replace('BRITISH VIRGIN ISLANDS','VIRGIN ISLANDS')
    data_csv['RegAddress.Country'] = data_csv['RegAddress.Country'].replace('U',np.NaN)
    data_csv['RegAddress.Country'] = data_csv['RegAddress.Country'].replace('SW7',np.NaN)
    data_csv['RegAddress.Country'] = data_csv['RegAddress.Country'].replace('SOUTH-WEST AFRICA','NAMIBIA')
    data_csv['RegAddress.Country'] = data_csv['RegAddress.Country'].replace('SOUTH AFRICAN','SOUTH AFRICA')
    data_csv['RegAddress.Country'] = data_csv['RegAddress.Country'].replace('PO33 2TG',np.NaN)
    data_csv['RegAddress.Country'] = data_csv['RegAddress.Country'].replace('NORTHERN IRELAND UNITED KINGDOM','NORTHERN IRELAND')
    data_csv['RegAddress.Country'] = data_csv['RegAddress.Country'].replace('N',np.NaN)
    data_csv['RegAddress.Country'] = data_csv['RegAddress.Country'].replace('LIVERPOOL',np.NaN)
    data_csv['RegAddress.Country'] = data_csv['RegAddress.Country'].replace('KY1-9005',np.NaN)
    data_csv['RegAddress.Country'] = data_csv['RegAddress.Country'].replace('HONG KONG CHINA','HONG KONG')
    data_csv['RegAddress.Country'] = data_csv['RegAddress.Country'].replace('GY1 4HY',np.NaN)
    data_csv['RegAddress.Country'] = data_csv['RegAddress.Country'].replace('GY1 2HL',np.NaN)
    data_csv['RegAddress.Country'] = data_csv['RegAddress.Country'].replace('FAROE ISLAND','FAROE ISLANDS')
    data_csv['RegAddress.Country'] = data_csv['RegAddress.Country'].replace('ENGLAND & WALES',np.NaN)
    data_csv['RegAddress.Country'] = data_csv['RegAddress.Country'].replace('EAST SUSSEX',np.NaN)
    data_csv['RegAddress.Country'] = data_csv['RegAddress.Country'].replace('DUMMY',np.NaN)
    data_csv['RegAddress.Country'] = data_csv['RegAddress.Country'].replace('CV31 3TG',np.NaN)
    data_csv['RegAddress.Country'] = data_csv['RegAddress.Country'].replace('CV31 3RG',np.NaN)
    data_csv['RegAddress.Country'] = data_csv['RegAddress.Country'].replace('CONGO',np.NaN)
    data_csv['RegAddress.Country'] = data_csv['RegAddress.Country'].replace('CHANNEL ISLES','CHANNEL ISLANDS')
    data_csv['RegAddress.Country'] = data_csv['RegAddress.Country'].replace('CHANNEL ISLANDSSW1W 0AU','CHANNEL ISLANDS')
    data_csv['RegAddress.Country'] = data_csv['RegAddress.Country'].replace('C',np.NaN)
    data_csv['RegAddress.Country'] = data_csv['RegAddress.Country'].replace('BWI',np.NaN)
    data_csv['RegAddress.Country'] = data_csv['RegAddress.Country'].replace('BRITISH CHANNEL ISLANDS','CHANNEL ISLANDS')
    data_csv['RegAddress.Country'] = data_csv['RegAddress.Country'].replace('1200',np.NaN)
    data_csv['RegAddress.Country'] = data_csv['RegAddress.Country'].replace('IRELAND','REPUBLIC OF IRELAND')
    data_csv['RegAddress.Country'] = data_csv['RegAddress.Country'].replace('USSR','RUSSIA')
    data_csv['RegAddress.Country'] = data_csv['RegAddress.Country'].replace('ENGLAND UNITED KINGDOM','UNITED KINGDOM')
    data_csv['RegAddress.Country'] = data_csv['RegAddress.Country'].replace('SCOTLAND  UK','SCOTLAND')
    ###################
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace(' ',np.NaN)
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('ANGOLA ','ANGOLA')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('BRITISH VIRGIN ISLANDS','VIRGIN ISLANDS')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('VIRGIN ISLANDS, BRITISH','VIRGIN ISLANDS')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('British Virgin Islands','VIRGIN ISLANDS')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('Cyprus','CYPRUS')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('ENGLAND & WALES','UNITED KINGDOM')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('ENGLAND AND WALES','UNITED KINGDOM')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('BRITISH','UNITED KINGDOM')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('Bulgaria','BULGARIA')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('FAROE IS','FAROE ISLANDS')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('Germany','GERMANY')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('Great Britain','UNITED KINGDOM')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('GREAT BRITAIN','UNITED KINGDOM')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('Ireland','IRELAND')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('IRELAND REP','IRELAND')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('IRISH REP','IRELAND')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('Isle of Man','ISLE OF MAN')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('Jersey','JERSEY')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('Luxembourg','LUXEMBOURG')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('Malta','MALTA')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('Netherlands','NETHERLANDS')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('Panama','PANAMA')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('Republic of Ireland','REPUBLIC OF IRELAND')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('ROI','IRELAND')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('SOUTH-WEST AFRICA','NAMIBIA')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('Spain','SPAIN')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('Turkey','TURKEY')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('Turkey    ','TURKEY')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('UK','UNITED KINGDOM')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('United Kingdom','UNITED KINGDOM')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('UNITED STATES','UNITED STATES OF AMERICA')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('United States of America','UNITED STATES OF AMERICA')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('USSR','RUSSIA')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('Panama    ','PANAMA')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('WEST GERMANY','GERMANY')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('BELARUS ','BELARUS')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('ENGLAND','UNITED KINGDOM')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('HOLLAND','NETHERLANDS')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('NORTHERN IRELAND','UNITED KINGDOM')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('OVERSEAS',np.NaN)
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('REPUBLIC OF NIGERIA','NIGERIA')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('WALLES','UNITED KINGDOM')
    data_csv['CountryOfOrigin'] = data_csv['CountryOfOrigin'].replace('WALES','UNITED KINGDOM')
process_country_features()


#Replace 'Non Supplied' with Nans
data_csv['SICCode.SicText_1'] = data_csv['SICCode.SicText_1'].replace('None Supplied', np.NaN)

#LimitedPartnerships.NumLimPartners should not contain negative values
data_csv['LimitedPartnerships.NumLimPartners'] = data_csv['LimitedPartnerships.NumLimPartners'].replace('-1', np.NaN)
data_csv['LimitedPartnerships.NumLimPartners'] = data_csv['LimitedPartnerships.NumLimPartners'].replace('-2', np.NaN)

# data_csv['incorpodariondate_D'], data_csv['incorpodariondate_M'] , data_csv['incorpodariondate_Y']=data_csv['IncorporationDate'].str.split('/', expand=True).rename(index=str, columns={0: "IncorporationDate_D", 1: "IncorporationDate_M", 2: "IncorporationDate_Y"})
# data_csv['IncorporationDate_parsed'] = pd.to_datetime(data_csv['IncorporationDate'], errors = 'coerce')
# data_csv['CompanyAge'] = data_csv['IncorporationDate_parsed']-pd.to_datetime('5/12/2018 00:00')



# Count Nans per column
total = data_csv.isnull().sum().sort_values(ascending=False)

# Percentage of nans per columns
percentage = ((data_csv.isnull().sum() / data_csv.isnull().count()) * 100).sort_values(ascending=False)

missing_train_data = pd.DataFrame({'FeatureName': total.index, 'MissingCountTotal': total.values, 'MissingCountPercent': percentage.values})

#  Build missing data table
if 1:

    trace = go.Table(
        header=dict(values=['Feature Name', 'Missing Count Total', 'Missing Count Percent'],
                    fill=dict(color='#C2D4FF'),
                    align=['left']),
        cells=dict(values=[missing_train_data["FeatureName"], missing_train_data["MissingCountTotal"],
                           missing_train_data["MissingCountPercent"]],
                   fill=dict(color='#F5F8FF'),
                   align=['left']))
    data = [trace]
    fig = go.Figure(data=data)
    py.plot(fig, filename='missing_train_data_table.html')

#  Build missing data bar plot
if 1:
    trace = go.Bar(
        y=missing_train_data["MissingCountPercent"],
        x=missing_train_data["FeatureName"],
    )
    data = [trace]
    layout = go.Layout(
        title="Missing data percentage per feature",
        xaxis=dict(title='Feature name', tickfont=dict(size=14)),
        yaxis=dict(title='NAN percentage', titlefont=dict(size=14), tickfont=dict(size=14))
    )
    fig = go.Figure(data=data, layout=layout)
    py.plot(fig, filename='FeatureNanPercentage.html')

#Count number of unique occurrences per columns
Nuniques = data_csv.nunique().sort_values(ascending=False)
Nuniques = pd.DataFrame({'FeatureName': Nuniques.index, 'UniqueCount': Nuniques.values})

#  Build unique count per column bar plot
if 1:
    trace = go.Bar(
        y=Nuniques["UniqueCount"],
        x=Nuniques["FeatureName"]
    )
    data = [trace]
    layout = go.Layout(
        title="Unique values per feature",
        xaxis=dict(title='Feature name', tickfont=dict(size=14)),
        yaxis=dict(title='Unique count', titlefont=dict(size=11), tickfont=dict(size=14))
    )
    fig = go.Figure(data=data, layout=layout)
    py.plot(fig, filename='FeatureUniqueCount.html')

# Drop DissolutionDate feature
data_csv.drop(columns=['DissolutionDate'], inplace=True)  # all missing data


data_csv.rename(columns=lambda x: x.replace(' PreviousName', 'PreviousName'), inplace=True) #contains an extra space



# Os prints sendo iguais eu garanto que nao hipotese de ter uma data de mudança de nome e não ter um previsous name, e vice versa
# The following lines are meant to verify if PreviousName_X.CompanyName and PreviousName_X.CONDATE are NAN for the same samples. If so, I will keep only PreviousName_X.CONDATE.

for i in range(1,11):
    print(np.sum(~pd.isnull(data_csv['PreviousName_'+str(i)+'.CompanyName']) * ~pd.isnull(data_csv['PreviousName_'+str(i)+'.CONDATE'])),
          np.sum(~pd.isnull(data_csv['PreviousName_'+str(i)+'.CompanyName'])),
          np.sum(~pd.isnull(data_csv['PreviousName_'+str(i)+'.CONDATE'])))

for i in range(1,11):
    data_csv.drop(columns=['PreviousName_'+str(i)+'.CompanyName'], inplace=True)



# uma vez que as features de mudança de nome tem muitos Nans vou criar uma feature que conta quantas vezes uma company mudou de nome
#Count number of nam changes per company
data_csv['ChangeNameCount'] = 1 * ~pd.isnull(data_csv['PreviousName_1.CONDATE']) + 1 * ~pd.isnull(
    data_csv['PreviousName_2.CONDATE']) + \
                              1 * ~pd.isnull(data_csv['PreviousName_3.CONDATE']) + 1 * ~pd.isnull(
    data_csv['PreviousName_4.CONDATE']) + 1 * ~pd.isnull(data_csv['PreviousName_5.CONDATE']) + \
                              1 * ~pd.isnull(data_csv['PreviousName_6.CONDATE']) + 1 * ~pd.isnull(
    data_csv['PreviousName_7.CONDATE']) + \
                              1 * ~pd.isnull(data_csv['PreviousName_8.CONDATE']) + 1 * ~pd.isnull(
    data_csv['PreviousName_9.CONDATE']) + 1 * ~pd.isnull(data_csv['PreviousName_10.CONDATE'])

#  Build number of name changes histogram
if 1:
    data = [go.Histogram(x=data_csv['ChangeNameCount'])]

    layout = go.Layout(title="How many companies changed their names X time?",
                       xaxis=dict(title='Name Changes', tickfont=dict(size=14)),
                       yaxis=dict(title='Number of Companies', titlefont=dict(size=16), tickfont=dict(size=16)))

    fig = go.Figure(data=data, layout=layout)
    py.plot(fig, filename='ChangeNameCount_Hist.html')

#  Build company status bar plot
if 1:
    temp = data_csv['CompanyStatus'].value_counts().sort_values(ascending=False)
    data = [go.Bar(x=temp.index, y=temp)]
    layout = go.Layout(title="Company Status Histogram",
                       xaxis=dict(title='Company Status', tickfont=dict(size=14)),
                       yaxis=dict(title='Number of Companies', titlefont=dict(size=14),
                                  tickfont=dict(size=14)))

    fig = go.Figure(data=data, layout=layout)
    py.plot(fig, filename='Company Status_Hist.html')


# Parse IncorporationDate and igore erros
data_csv['IncorporationDate_parsed'] = pd.to_datetime(data_csv['IncorporationDate'], errors='coerce')

#Calculate companies age
data_csv['CompanyAge_years'] = 2018 - data_csv['IncorporationDate_parsed'].dt.year

# Pandas does not support Timestamps for these tow samples, so I process them manually. These are the two oldest companies in the data set
data_csv['CompanyAge_years'][data_csv[' CompanyNumber'] == 'RC000830'] = 2018 - 1552
data_csv['CompanyAge_years'][data_csv[' CompanyNumber'] == 'RC000892'] = 2018 - 1638


#  Build companies age histogram
if 1:
    data = [go.Histogram(x=data_csv['CompanyAge_years'])]
    layout = go.Layout(title="Company age (years) Histogram",
                       xaxis=dict(title='Years', tickfont=dict(size=14)),
                       yaxis=dict(title='Number of Companies', titlefont=dict(size=14), tickfont=dict(size=14)))
    fig = go.Figure(data=data, layout=layout)
    py.plot(fig, filename='CompanyAge_years_Hist.html')


if 0:
    trace = go.Table(
        header=dict(values=['Age(years)', 'Count'],
                    fill=dict(color='#C2D4FF'),
                    align=['left'] * 5),
        cells=dict(values=[data_csv['CompanyAge_years'].value_counts().sort_index().index,
                           data_csv['CompanyAge_years'].value_counts().sort_index()],
                   fill=dict(color='#F5F8FF'),
                   align=['left'] * 5))

    data = [trace]
    fig = go.Figure(data=data)
    py.plot(fig, filename='Company_age_years_table.html')


#Create sub-datasets w.r.t. company age
AgeLessthan1 = np.sum(data_csv['CompanyAge_years'] < 1)
Age1 = np.sum(data_csv['CompanyAge_years'] == 1)
Age2 = np.sum(data_csv['CompanyAge_years'] == 2)
Age3 = np.sum(data_csv['CompanyAge_years'] == 3)
Age4 = np.sum(data_csv['CompanyAge_years'] == 4)
Age5 = np.sum(data_csv['CompanyAge_years'] == 5)
Age6 = np.sum(data_csv['CompanyAge_years'] == 6)
Age7 = np.sum(data_csv['CompanyAge_years'] == 7)
Age8 = np.sum(data_csv['CompanyAge_years'] == 8)
Age9 = np.sum(data_csv['CompanyAge_years'] == 9)
AgeMorethan10 = np.sum(data_csv['CompanyAge_years'] >= 10)

company_ages = [AgeLessthan1, Age1, Age2, Age3, Age4, Age5,Age6,Age7,Age8,Age9, AgeMorethan10]
#  Build companies age per groups pie chart
if 1:
    labels = [ 'Less than 1 year', '1 year', '2 years', '3 years', '4 years', '5 years','6 years','7 years','8 years','9 years', 'More than 10 years']
    values = company_ages

    trace = go.Pie(labels=labels, values=values)

    py.plot([trace], filename='Company_age_pie_chart.html')


# Calculate how many SICCodes each company has
data_csv['SICCode_many'] = ~data_csv['SICCode.SicText_1'].isnull() * 1 + ~data_csv['SICCode.SicText_2'].isnull() * 1 + ~ \
data_csv['SICCode.SicText_3'].isnull() * 1 + ~data_csv['SICCode.SicText_4'].isnull() * 1


# Build hitogram of how many SICCodes each company has
if 1:
    data = [go.Histogram(x=data_csv['SICCode_many'])]
    layout = go.Layout(title="SICCode_many",
                       xaxis=dict(title='Number of SICCODES', tickfont=dict(size=14)),
                       yaxis=dict(title='Number of Companies', titlefont=dict(size=14),
                                  tickfont=dict(size=14)))
    fig = go.Figure(data=data, layout=layout)
    py.plot(fig, filename='SICCode_many_Hist.html')

# Load SICCodes list from http://resources.companieshouse.gov.uk/sic/
siccodes = pd.read_csv('siccodes.csv', low_memory=False, delimiter=';')
siccodes["Code"] = pd.to_numeric(siccodes["Code"])


#Concatenate four columns of SICCodes
conjunto = pd.concat([data_csv['SICCode.SicText_1'], data_csv['SICCode.SicText_2'], data_csv['SICCode.SicText_3'], data_csv['SICCode.SicText_4']])
# Split code and description
conjunto2=conjunto.str.split(' - ', expand=True).rename( index=str, columns={0: "SICCode.SicCODE", 1: "SICCode.SicTEXTO"})
conjunto2["SICCode.SicCODE"] = pd.to_numeric(conjunto2["SICCode.SicCODE"])
# Join dataset information and SICCodes table from http://resources.companieshouse.gov.uk/sic/ in order to get the section
conjunto3=pd.merge(conjunto2, siccodes, left_on="SICCode.SicCODE", right_on="Code")

# Build bar plot of the SICCodes sections
if 1:
    temp = conjunto3['Description'].value_counts().sort_values(ascending=False)
    data = [go.Bar(x=temp.index, y=temp)]
    layout = go.Layout(title="SICCodes distribution by section",
                       xaxis=dict(title='SICCode Sections', tickfont=dict(size=14)),
                       yaxis=dict(title='Number of Companies', titlefont=dict(size=14),
                                  tickfont=dict(size=14)))

    fig = go.Figure(data=data, layout=layout)
    py.plot(fig, filename='SICCodes_by_sectiont.html')


if 0:
    trace = go.Table(
        header=dict(values=['SICCode section', 'Count'],
                    fill=dict(color='#C2D4FF'),
                    align=['left'] * 5),
        cells=dict(values=[conjunto3['Description'].value_counts().sort_values(ascending=False).index,
                           conjunto3['Description'].value_counts().sort_values(ascending=False)],
                   fill=dict(color='#F5F8FF'),
                   align=['left'] * 5))

    data = [trace]
    fig = go.Figure(data=data)
    py.plot(fig, filename='SICCode_section_table.html')

# Load country code 3-digit codes
countrycodes = pd.read_csv('countrycodeslist.csv', delimiter=',')
CountryOfOrigin_valuecount=data_csv['CountryOfOrigin'].value_counts().sort_values(ascending=True)

newDF = pd.DataFrame()
newDF['CoutryIndex']=CountryOfOrigin_valuecount.index
newDF['CountryCount']=CountryOfOrigin_valuecount.values

#Map country codes and country name
CountryInfo=pd.merge(newDF, countrycodes, left_on="CoutryIndex", right_on="CountryName")

# Build density map of companies per country with UK
if 1:
    data = [ dict(
            type = 'choropleth',
            locations = CountryInfo['CountryCode'].values,
            z = CountryInfo['CountryCount'].values,
            text=CountryInfo['CountryName'].values,
            locationmode='ISO-3',
            colorscale='Red',
            marker=dict(line=dict(width=0.7)),
        colorbar=dict(autotick=False, tickprefix='', title='Number of Companies'),
    )]

    layout = dict(
        title = 'Destribution of compnies by country of origin (With UK)',
        geo = dict(
            showframe = False,
            showcoastlines = False,
            projection = dict(type = 'Mercator')))

    fig = dict( data=data, layout=layout )
    py.plot( fig, validate=False, filename='d3-world-map.html' )

# Build density map of companies per country without UK
if 0:
    CountryInfo_novo=CountryInfo.drop([157])
    data = [ dict(
            type = 'choropleth',
            locations = CountryInfo_novo['CountryCode'].values,
            text=CountryInfo_novo['CountryName'].values,
            z = CountryInfo_novo['CountryCount'].values,
            locationmode='ISO-3',
            colorscale='Red',
            marker=dict(line=dict(width=0.7)),
        colorbar=dict(autotick=False, tickprefix='', title='Number of Companies'),
    )]

    layout = dict(
        title = 'Destribution of compnies by country of origin (With UK) (Without UK)',
        geo = dict(
            showframe = False,
            showcoastlines = False,
            projection = dict(type = 'Mercator')))

    fig = dict( data=data, layout=layout )
    py.plot( fig, validate=False, filename='d3-world-map_without_UK.html' )

#Create subset of data only with companies from UK and Active
data_UK=data_csv[(((data_csv['CountryOfOrigin']=='UNITED KINGDOM')*1) * ((data_csv['CompanyStatus']=='Active')*1)==1)]

# Create subsets of data w.r.t age
data_UK_AgeLessthan1 = data_UK[data_UK['CompanyAge_years'] < 1]
data_UK_Age1 = data_UK[data_UK['CompanyAge_years'] == 1]
data_UK_Age2 = data_UK[data_UK['CompanyAge_years'] == 2]
data_UK_Age3 = data_UK[data_UK['CompanyAge_years'] == 3]
data_UK_Age4 = data_UK[data_UK['CompanyAge_years'] == 4]
data_UK_Age5 = data_UK[data_UK['CompanyAge_years'] == 5]
data_UK_Age6 = data_UK[data_UK['CompanyAge_years'] == 6]
data_UK_Age7 = data_UK[data_UK['CompanyAge_years'] == 7]
data_UK_Age8 = data_UK[data_UK['CompanyAge_years'] == 8]
data_UK_Age9 = data_UK[data_UK['CompanyAge_years'] == 9]
data_UK_AgeMorethan10 = data_UK[data_UK['CompanyAge_years'] >= 10]

def plotbarsiccodes(data):
    conjunto = pd.concat([data['SICCode.SicText_1'], data['SICCode.SicText_2'], data['SICCode.SicText_3'], data['SICCode.SicText_4']])
    conjunto2=conjunto.str.split(' - ', expand=True).rename( index=str, columns={0: "SICCode.SicCODE", 1: "SICCode.SicTEXTO"})
    conjunto2["SICCode.SicCODE"] = pd.to_numeric(conjunto2["SICCode.SicCODE"])
    conjunto3=pd.merge(conjunto2, siccodes, left_on="SICCode.SicCODE", right_on="Code")
    return conjunto3


lista_categorias_sic=siccodes['Description'].unique()
dictionary=dict()
data=list()
for cat in lista_categorias_sic:
    d = {'Age': ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '>=10'], 'Count': [plotbarsiccodes(data_UK_AgeLessthan1)['Description'].value_counts().sort_values(ascending=False)[cat],
                                                                                     plotbarsiccodes(data_UK_Age1)['Description'].value_counts().sort_values(ascending=False)[cat],
                                                                                     plotbarsiccodes(data_UK_Age2)['Description'].value_counts().sort_values(ascending=False)[cat],
                                                                                     plotbarsiccodes(data_UK_Age3)['Description'].value_counts().sort_values(ascending=False)[cat],
                                                                                     plotbarsiccodes(data_UK_Age4)['Description'].value_counts().sort_values(ascending=False)[cat],
                                                                                     plotbarsiccodes(data_UK_Age5)['Description'].value_counts().sort_values(ascending=False)[cat],
                                                                                     plotbarsiccodes(data_UK_Age6)['Description'].value_counts().sort_values(ascending=False)[cat],
                                                                                     plotbarsiccodes(data_UK_Age7)['Description'].value_counts().sort_values(ascending=False)[cat],
                                                                                     plotbarsiccodes(data_UK_Age8)['Description'].value_counts().sort_values(ascending=False)[cat],
                                                                                     plotbarsiccodes(data_UK_Age9)['Description'].value_counts().sort_values(ascending=False)[cat],
                                                                                     plotbarsiccodes(data_UK_AgeMorethan10)['Description'].value_counts().sort_values(ascending=False)[cat]]}
    dictionary[cat]=pd.DataFrame(data=d)
    trace = go.Scatter(x=dictionary[cat]['Age'], y=dictionary[cat]['Count'], mode = 'lines', name = cat)
    data.append(trace)


# set x-axis labels and their corresponding data values
labels = ['2018', '2017', '2016', '2015','2014', '2013', '2012', '2011', '2010', '2009', '2008']
tickvals = [0, 1, 2, 3,4,5,6,7,8,9,10]

layout = go.Layout(title="Number of companies per year per SICCode section",
                   xaxis=go.layout.XAxis(
                       ticktext=labels,
                       tickvals=tickvals
                   ),
                       yaxis=dict(title='Number of Companies', titlefont=dict(size=14),
                                  tickfont=dict(size=14), range=[0,105000]))
fig = go.Figure(data=data, layout=layout)
py.plot(fig, filename='graficol.html')



